FROM python:3.7-alpine

COPY ./www /www

CMD [ "python3", "/www/pyserver.py"]
